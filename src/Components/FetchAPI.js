import { Component } from "react";

class FetchAPI extends Component {
    FetchAPI = async (url, body) => {
        let response = await fetch(url, body);
        let json = await response.json();
        return json;
    }

    getAllOrder = () => {
        this.FetchAPI("http://203.171.20.210:8080/devcamp-pizza365/orders").then((data) => {
            console.log(data)
        })
    }

    createOrder = () => {
        let body = {
            method: "POST",
            body: JSON.stringify({
                id: 19456,
                orderCode: "MG1p7dTGCT",
                kichCo: "Large",
                duongKinh: "30",
                suon: 8,
                salad: 400,
                loaiPizza: "Pizza Hawaii",
                idVourcher: 12354,
                thanhTien: 250000,
                giamGia: 25000,
                idLoaiNuocUong: "LAVIE",
                soLuongNuoc: 2,
                hoTen: "Bao Khuyen",
                email: "khuyentb@devcamp.edu.vn",
                soDienThoai: 1234567890,
                diaChi: "Hà Nội",
                loiNhan: "ko co",
                trangThai: "open",
                ngayTao: 1665134514993,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }

        this.FetchAPI("http://203.171.20.210:8080/devcamp-pizza365/orders", body).then((data) => {
            console.log(data);
        })
    }

    getOrderByID = () => {
        this.FetchAPI("http://203.171.20.210:8080/devcamp-pizza365/orders/vYLjTYVJRY").then((data) => {
            console.log(data)
        })
    }

    updateOrder = () => {
        let body = {
            method: "PUT",
            body: JSON.stringify({
                id: 3722,
                orderCode: "MG1p7dTGCT",
                kichCo: "Large",
                duongKinh: "30",
                suon: 8,
                salad: 400,
                loaiPizza: "Pizza Hawaii",
                idVourcher: 12354,
                thanhTien: 250000,
                giamGia: 25000,
                idLoaiNuocUong: "LAVIE",
                soLuongNuoc: 2,
                hoTen: "Bao Khuyen",
                email: "khuyentb@devcamp.edu.vn",
                soDienThoai: 1234567890,
                diaChi: "Hà Nội",
                loiNhan: "hehehehe",
                trangThai: "open",
                ngayTao: 1665134514993,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }

        this.FetchAPI("http://203.171.20.210:8080/devcamp-pizza365/orders/3593", body).then((data) => {
            console.log(data);
        })
    }

    checkVoucherByID = () => {
        this.FetchAPI("http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/24864").then((data) => {
            console.log(data);
        })

    }

    getDrinkList = () => {
        this.FetchAPI("http://203.171.20.210:8080/devcamp-pizza365/drinks").then((data) => {
            console.log(data);
        })
    }

    render() {
        return (
            <div className="row m-5">
                <div className="col"><button className="btn btn-info" onClick={this.getAllOrder}>Call api get all orders!</button></div>
                <div className="col"><button className="btn btn-success" onClick={this.createOrder}>Call api create order!</button></div>
                <div className="col"><button className="btn btn-warning" onClick={this.getOrderByID}>Call api get order by id!</button></div>
                <div className="col"><button className="btn btn-primary" onClick={this.updateOrder}>Call api update order!</button></div>
                <div className="col"><button className="btn btn-info" onClick={this.checkVoucherByID}>Call api voucher by id!</button></div>
                <div className="col"><button className="btn btn-danger" onClick={this.getDrinkList}>Call api Get drink list!</button></div>
            </div>
        )
    }
}

export default FetchAPI