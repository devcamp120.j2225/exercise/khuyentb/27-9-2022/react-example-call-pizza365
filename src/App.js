import "bootstrap/dist/css/bootstrap.min.css"
import AxiosLibrary from "./Components/Axios";
import FetchAPI from "./Components/FetchAPI";


function App() {
  return (
    <div className="App">
      <FetchAPI></FetchAPI>
      <AxiosLibrary></AxiosLibrary>
    </div>
  );
}

export default App;
